﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COPMDU.Domain.Entity
{
    public class ResultadoLogin
    {
        /// <summary>
        /// Login não foi realizado
        /// </summary>
        public static ResultadoLogin NaoLogado => new ResultadoLogin() { Logado = false };

        public bool Logado { get; set; }

        public bool CaptchaOk { get; set; }

        public string Usuario { get; set; }

        public string NomeUsuario { get; set; }

        public DateTime? UltimoAcesso { get; set; }

        public bool Expirado
        {
            get
            {
                return UltimoAcesso.HasValue &&
                    DateTime.Now.Subtract(UltimoAcesso.Value).TotalSeconds > 120;
            }
        }
    }

}
