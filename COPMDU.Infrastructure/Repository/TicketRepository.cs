﻿using COPMDU.Domain.Entity;
using COPMDU.Infrastructure.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using COPMDU.Infrastructure.Util;
using COPMDU.Infrastructure.ClassAttribute;
using COPMDU.Domain;
using System.Linq;
using System.Xml.Serialization;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using COPMDU.Infrastructure.InterationException;

namespace COPMDU.Infrastructure.Repository
{
    public class TicketRepository : ITicketRepository
    {
        private readonly IConfiguration _config;
        private readonly ILogger _logger;
        private readonly CopMduDbContext _db;
        private readonly ILogRepository _logRepository;
        private readonly IFrontMdu _frontMdu;
        private readonly DbHelper _dbHelper;

        private const string NOK_AuthenticateNM = "Não foi possível autenticar o usuário {0} no atlas";
        private const string NOK_GetAllTechnicianNM = "Não foi possível obter os tickets do técnico";
        private const string NOK_GetTicketNM = "Não foi possível obter os dados do ticket";
        private const string NOK_AttachEvidenceNM = "Não foi possível anexar a evidência";
        private const string NOK_CloseNM = "Não foi possível fechar o ticket";

        private readonly PageSession _client, _clientQN;

        private bool DontClose => bool.Parse(_config["CopMdu:DontCloseTicket"]);

        private bool ReturnInformative => bool.Parse(_config["CopMdu:ReturnInformative"]);
        
        private int Timezone => int.Parse(_config["CopMdu:Timezone"]);

        private bool LoadAdditionalFront => bool.Parse(_config["CopMdu:EnableCollectFrontMdu"]);

        private string MainUrl => _config["Url:NewMonitor:Uri"];

        private string MainUrlQN => _config["Url:Qualinet:Uri"];
        private string Proxy => _config["Url:NewMonitor:UseProxy"];

        private string ProxyQN => _config["Url:Qualinet:UseProxy"];

        public TicketRepository(IConfiguration config, CopMduDbContext db, ILogger<TicketRepository> logger, ILogRepository logRepository, IFrontMdu frontMdu)
        {
            _db = db;
            _logger = logger;
            _config = config;
            _logRepository = logRepository;
            _frontMdu = frontMdu;
            _dbHelper = new DbHelper(db, config);
            _client = new PageSession(MainUrl, config, db);
            if (!string.IsNullOrEmpty(Proxy))
                _client.SetProxy(Proxy);

            _clientQN = new PageSession(MainUrlQN, config, db);
            if (!string.IsNullOrEmpty(ProxyQN))
                _clientQN.SetProxy(ProxyQN);

            _clientQN.SetCredentials(_config["Credentials:Qualinet:User"], _config["Credentials:Qualinet:Password"]);
        }

        enum AuthenticateResponse
        {
            [PagePossibleResult(ContainString = "index.php")]
            Valid,
            [PagePossibleResult(ContainString = "Senha incorreta")]
            IncorretPassword,
            [PagePossibleResult(ContainString = "Login não encontrado")]
            UserNotFound,
            [PagePossibleResult(ContainString = "")]
            Invalid
        }

        public async Task<User> Authenticate(string username, string password)
        {
            try
            {
                var path = _config["Url:NewMonitor:Path:Account"];
                var content = await _client.PostWithValidation<AuthenticateResponse>(path, new[] {
                    ("xajax", "validaUsuario"),
                    ("xajaxr", AjaxDate),
                    ("xajaxargs[]", username),
                    ("xajaxargs[]", password),
                });

                var authenticated = content == AuthenticateResponse.Valid ? "OK" : "NOK";

                return new User() { Username = username, Email = String.Empty, Authenticated = authenticated };
            }
            catch (Exception ex)
            {
                Log("TicketRepository.Authenticate", $"Erro: {ex.Message}");
                throw new TicketResponseException(string.Format(NOK_AuthenticateNM, username), ex);
            }
        }

        public IEnumerable<Outage> GetAllTechnician(int technicianId)
        {
            try
            {
                var outages = _db.Outage
                    .Include(o => o.Symptom)
                    .Include(o => o.City)
                    .OrderBy(o => o.Id)
                    .Where(o =>
                    new[] { ReturnInformative? 1: -1, 2, 3, 4 }.Contains(o.SymptomId) &&
                    // !o.Filtered &&
                    // o.City.EnabledInteration && 
                    // o.Contract > 0 &&
                    // o.Notification > 0 &&
                    o.Active 
                    );

                if (bool.Parse(_config["CopMdu:EnableTechnicianFilter"]) && technicianId > 1)
                    outages = outages.Where(o => o.TechnicUserId == technicianId);

                return outages
                    .ToList();
            }
            catch (Exception ex)
            {
                Log("TicketRepository.GetAllTechnician", $"Erro: {ex.Message}");
                throw new TicketResponseException(NOK_GetAllTechnicianNM, ex);
            }
        }

        public Outage Get(int id)
        {
            try
            {
                var outage = _db.Outage
                    .Include(o => o.City)
                    .Include(o => o.Type)
                    .Include(o => o.TechnicUser)
                    .Include(o => o.Signals)
                        .ThenInclude(s => s.Status)
                    .Include(o => o.Signals)
                        .ThenInclude(s => s.Terminal)
                    .Include(o => o.Signals)
                        .ThenInclude(s => s.CmRx)
                    .Include(o => o.Signals)
                        .ThenInclude(s => s.CmSnr)
                    .Include(o => o.AffectedServices)
                        .ThenInclude(s => s.Service)
                    .FirstOrDefault(o => o.Id == id);

                return outage;
            }
            catch (Exception ex)
            {
                Log("TicketRepository.Get", $"Erro: {ex.Message}");
                throw new TicketResponseException(NOK_GetTicketNM, ex);
            }
        }

        public async Task<string> AttachEvidence(int id, byte[] fileData)
        {
            try
            {
                var path = _config["Url:NewMonitor:Path:OutageUpload"];

                var data = new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture))
                {
                    { new StringContent(id.ToString()), "id_ticket" },
                    { new StreamContent(new MemoryStream(fileData)), "arqEvid", $"evidencia_copmdu_{DateTime.Now.ToString("yyyyMMddHHmmss")}.pdf" }
                };

                var content = await _client.Post(path, data);

                return content;
            }
            catch (Exception ex)
            {
                Log("TicketRepository.AttachEvidence", $"Erro: {ex.Message}");
                throw new TicketResponseException(NOK_AttachEvidenceNM, ex);
            }
        }

        private void Log(string source, string message, int? ticket = null, int type = 0)
        {
            _logger.LogInformation(message);
            _logRepository.Add(new Log()
            {
                Message = message,
                Source = source,
                Ticket = ticket,
                Type = type,
            });

        }

        public async Task CloseDb(int outageId, int userId)
        {
            var outage = _db.Outage.Find(outageId);
            outage.Active = false;
            outage.ClosedById = userId;
            _db.Update(outage);
            await _db.SaveChangesAsync();
        }

        public async Task<CloseStatus> Close(int ticket, int idResolution, string description, DateTime closeTime, int user)
        {
            try
            {
                closeTime = closeTime.AddMinutes(Timezone).AddMinutes(-5);
                var path = String.Format(_config["Url:NewMonitor:Path:OutageView"], ticket);
                _logRepository.Add(new Log { UserId = user, Message = $"Fechamento de ticket {ticket} com resolucao {idResolution} e descricao: {description} as {closeTime}", Ticket = ticket  });
                var data = new[] {
                    ("xajax", "FechaTicket"),
                    ("xajaxr", AjaxDate),
                    ("xajaxargs[]", "N"),
                    ("xajaxargs[]", "216"),
                    ("xajaxargs[]", idResolution.ToString()),
                    ("xajaxargs[]", description),
                    ("xajaxargs[]", ""),
                    ("xajaxargs[]", closeTime.ToString("dd/MM/yyyy")),
                    ("xajaxargs[]", closeTime.ToString("HH:mm")),
                    ("xajaxargs[]", "1"),
                    ("xajaxargs[]", "1"),
                    ("xajaxargs[]", "1"),//Natureza da ocorrencia
                    ("xajaxargs[]", "1"),
                    ("xajaxargs[]", "1"),
                    ("xajaxargs[]", "1"),
                    ("xajaxargs[]", "1"),
                    ("xajaxargs[]", "2"),
                    ("xajaxargs[]", "1"),
                    ("xajaxargs[]", ""),
                    ("xajaxargs[]", "3"),
                    ("xajaxargs[]", "59"),
                    ("xajaxargs[]", ""),
                    ("xajaxargs[]", ""),
                    ("xajaxargs[]", "N"),
                    ("xajaxargs[]", "0"),
                    ("xajaxargs[]", ""),
                };
                if (DontClose)
                    return CloseStatus.ClosedSuccessfully;

                var result = await _client.Post(path, data);

                if (result.Contains("sucesso"))
                {
                    Log("TicketRepository.Close", $"Fechado o ticket {ticket} com sucesso");
                    return CloseStatus.ClosedSuccessfully;
                }
                Log("TicketRepository.Close", $"Falha ao tentar fechar o ticket {ticket} recebeu do servidor {result}", ticket, 2);
                return CloseStatus.Failed;
            }
            catch (Exception ex)
            {
                Log("TicketRepository.Close", $"Erro: {ex.Message}");
                throw new TicketResponseException(NOK_CloseNM, ex);
            }
        }

        public enum CloseResult
        {
            [PagePossibleResult(ContainString = "Não foi possível finalizar o ticket")]
            NotPossible,
            [PagePossibleResult(ContainString = "Data/Hora final deve ser posterior a inicial")]
            InvalidCloseTime,
            [PagePossibleResult(ContainString = "sucesso")]
            Ok,
            [PagePossibleResult(ContainString = "")]
            Unknown
        }


        public async Task<List<TicketResponse>> GetTicketsNM()
        {
            var path = _config["Url:NewMonitor:Path:TicketOpen"];

            var content = await _client.Get(path);

            var tickets = JsonConvert.DeserializeObject<Tickets>(content);
            return tickets.Data.ToList();
        }

        public async Task<List<ServiceOrder>> GetServiceOrders()
        {
            var path = _config["Url:Qualinet:Path:ServiceOrderOpen"];

            var content = await _clientQN.Get(path);

            XmlSerializer serializer = new XmlSerializer(typeof(ServiceOrderCollection));

            List<ServiceOrder> orders;
            using (var stringReader = new StringReader(content))
                orders = ((ServiceOrderCollection)serializer.Deserialize(stringReader)).ServiceOrders;
            return orders;
        }

        public static string AjaxDate =>
            JavascriptInteration.CurrentDateToJavascriptDate().ToString();

        public async Task<OutageResponse> GetTicketDetail(int id) =>
            await _client.PostConverted<OutageResponse>(
                String.Format(_config["Url:NewMonitor:Path:OutageView"], id),
                new[] {
                    ("xajax", "mostraTicket"),
                    ("xajaxr", AjaxDate)
                });

        /// <summary>
        /// Retira os tickets que não são controlados
        /// </summary>
        private async Task<List<TicketResponse>> GetFilteredTicket() =>
            (await GetTicketsNM()).FindAll(t => t.titulo == "MDU");

        private void RemoveDuplicatedId(List<Outage> data) 
        {
            var lastId = 0;
            foreach (var item in data.OrderBy(d => d.Id))
            {
                if (lastId == item.Id)
                    data.Remove(item);
                else
                    lastId = item.Id;
            }
        }

        public async Task<List<TicketFrontData>> GetFrontTickets(IEnumerable<int> ids)
        {
            var frontDatas = new List<TicketFrontData>();
            //foreach (var id in ids)
            //{
            //    var data = await _frontMdu.GetDetail(id);
            //    if (data != null)
            //        frontDatas.Add(data);
            //}
            var tasks = ids.Select(async (id) =>
            {
                var data = await _frontMdu.GetDetail(id);
                if (data != null)
                    frontDatas.Add(data);
            });
            await Task.WhenAll(tasks);
            return frontDatas;
        }

        public async Task LoadAdditionalData(List<Outage> outages)
        {
            var filteredOutages = outages
                .Where(o => (o.TechnicUser == null || o.Notification == 0) && o.StartDate >= DateTime.Now.AddHours(-48));
            var users = _db.User.ToList();

            var frontDatas = await GetFrontTickets(filteredOutages.Select(o => o.Id));
            foreach (var data in frontDatas)
            {
                var outage = outages.First(o => o.Id == data.Id);
                if (outage.Notification == 0 && data.Notificacao > 0 && data.Notificacao != outage.Contract)
                    outage.Notification = data.Notificacao;
                if (outage.TechnicUser == null && data.Tecnico.Trim() != "")
                {
                    outage.TechnicUser = _dbHelper.ReturnUser(data.Tecnico, users, _db.User);
                    outage.TechnicianUpdatedTime = DateTime.Now;
                }
                outage.NotificationFromAtlas = true;
            }
        }

        /// <summary>
        /// Carrega novas outages cadastradas e fecha as que foram fechadas
        /// </summary>
        /// <returns></returns>
        public async Task<bool> LoadNewOutage()
        {
            var tickets = await GetFilteredTicket();
            var dbOutages = _db.Outage;

            var ticketIds = tickets.Select(t => int.Parse(t.ticket)).ToList();
            // dados a inativar
            await dbOutages
                .Where(o => !ticketIds.Contains(o.Id) && o.Active)
                .ForEachAsync(o => o.Active = false);
            // dados a ativar
            await dbOutages
                .Where(o => ticketIds.Contains(o.Id) && !o.Active)
                .ForEachAsync(o => o.Active = true);
            await _db.SaveChangesAsync();

            var dbIds = dbOutages
                .Where(o => o.Active && o.ClosedById == null)
                .Select(o => o.Id)
                .ToList();

            var dbIdsWithTechnician = dbOutages
                .Where(o => o.Active && o.TechnicUserId == null && o.ClosedById == null)
                .Select(o => o.Id)
                .ToList();

            var serviceOrders = await GetServiceOrders();

            var treatment = new DataTreatment(_db, _config, _dbHelper);
            var loadStartTime = DateTime.Now;

            var loadedOutages = treatment.TreatList(tickets, serviceOrders);

            if (LoadAdditionalFront)
                await LoadAdditionalData(loadedOutages);

            var loadEndTime = DateTime.Now;
            // dados a inserir
            var newOutages = loadedOutages
                .Where(o => !dbIds.Contains(o.Id))
                .ToList();

            // dados que irao atualziar
            var updatedOutages = loadedOutages
                .Where(o => dbIds.Contains(o.Id))
                .ToList();
            // não atualiza dados de serviços afetados
            updatedOutages.ForEach(o => {
                o.AffectedServices = null;
                o.SymptomId = o.Symptom.Id;
                o.TechnicUserId = o.TechnicUser?.Id;
            });

            RemoveDuplicatedId(newOutages);
            _db.AddRange(newOutages);
            _db.UpdateRange(updatedOutages);

            // remove dados que só são atualizados na inserção
            foreach (var updatedOutage in updatedOutages)
                _db.Entry(updatedOutage).Property(o => o.InsertDate).IsModified = false;

            foreach (var updatedOutage in updatedOutages.Where(o => dbIdsWithTechnician.Contains(o.Id)))
                _db.Entry(updatedOutage).Property(o => o.TechnicianUpdatedTime).IsModified = false;

            await _db.SaveChangesAsync();

            var log =
                $"{DateTime.Now} - Efetuado carga agora de dados encontrado {tickets.Count} tickets do servidor do NM\n" +
                $"{newOutages.Count} outages foram adicionados\n" +
                $"{updatedOutages.Count} outages foram atualizados\n" +
                $"Data carregada em {(loadEndTime - loadStartTime).TotalMilliseconds}ms";
            _logger.LogInformation(log);
            _logRepository.Add(new Log()
            {
                Message = log,
                Source = "TicketRepository.LoadNewOutage"
            });

            return true;
        }

        public async Task InsertAll(List<Outage> outages)
        {
            await _db.AddRangeAsync(outages);
            _db.SaveChanges();
        }

        public IEnumerable<Outage> GetAllClosed(int technicianId)
        {
            try
            {
                var outages = _db.Outage
                    .Include(o => o.Symptom)
                    .Include(o => o.City)
                    .OrderBy(o => o.Id)
                    .Where(o => o.ClosedById == technicianId &&
                    // !o.Filtered &&
                    // o.City.EnabledInteration && 
                    // o.Contract > 0 &&
                    // o.Notification > 0 &&
                    !o.Active && 
                    o.TechnicianUpdatedTime.HasValue &&
                    o.TechnicianUpdatedTime.Value.ToShortDateString() == DateTime.Now.ToShortDateString()
                    );

                return outages
                    .ToList();
            }
            catch (Exception ex)
            {
                Log("TicketRepository.GetAllClosed", $"Erro: {ex.Message}");
                throw new TicketResponseException(NOK_GetAllTechnicianNM, ex);
            }
        }
    }
}
