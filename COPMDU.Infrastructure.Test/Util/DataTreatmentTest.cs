﻿using COPMDU.Infrastructure.Util;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace COPMDU.Infrastructure.Test.Util
{
    public class DataTreatmentTest
    {
        [Fact]
        public void GetDictionaryPositionTest()
        {
            Assert.Equal(4, DataTreatment.GetDictionaryPosition("Dado_4"));
            Assert.Equal(0, DataTreatment.GetDictionaryPosition("Dado"));
        }
    }
}
